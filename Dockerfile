FROM wordpress:php8.0-apache

# https://github.com/mlocati/docker-php-extension-installer#supported-php-extensions
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions

# https://stackoverflow.com/questions/48152299/alpine-locales-for-docker-image-php-gettext
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && \
	apt-get install -y nano apt-utils libxml2-dev zip libzip-dev wget cron libapache2-mod-security2 && \
	install-php-extensions soap zip gettext imagick redis pdo_mysql opcache && \
	apt-get clean -y && \
	rm -rf /var/lib/apt/lists/*

# Copy ANY configurations
COPY conf.d/ /usr/local/etc/php/conf.d/

COPY startup.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/startup.sh

COPY default-ssl.conf /etc/apache2/sites-available/
#COPY ssl.conf /etc/apache2/mods-available/ssl.conf

## SSL local dev

# selfsigned certs
RUN mkdir -p /etc/apache2/ssl/
RUN openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj \
	"/C=IT/ST=FC/L=CESENA/O=OIBI.DEV/CN=localhost" \
	-keyout /etc/apache2/ssl/ssl.key -out /etc/apache2/ssl/ssl.crt

# trusted cert but no luck with domain names
#RUN cp /etc/apache2/ssl/ssl.crt /usr/local/share/ca-certificates/
#RUN update-ca-certificates

## END SSL

RUN echo "insecure" >> $HOME/.curlrc

## enable self signed ssl
RUN a2enmod rewrite ssl security2
RUN a2ensite default-ssl

# ping crontab
RUN echo "* * * * * curl http://127.0.0.1/wp-cron.php?doing_wp_cron" >> /tmp/tmpcron && \
	crontab -u www-data /tmp/tmpcron && rm /tmp/tmpcron


ENTRYPOINT ["startup.sh"]
CMD ["apache2-foreground"]

ARG MAINTAINER
ARG BUILD_DATE
ARG CI_COMMIT_SHORT_SHA
ARG BUILD_VERSION

ENV MAINTAINER $MAINTAINER
ENV BUILD_DATE $BUILD_DATE
ENV CI_COMMIT_SHORT_SHA $CI_COMMIT_SHORT_SHA
ENV BUILD_VERSION $BUILD_VERSION

LABEL name="OiBi WordPress Ready2Go"
LABEL version="$BUILD_VERSION-$CI_COMMIT_SHORT_SHA"

EXPOSE 80 9003